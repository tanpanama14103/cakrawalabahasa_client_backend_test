<?php

use Illuminate\Database\Migrations\Migration;
use Illuminate\Database\Schema\Blueprint;
use Illuminate\Support\Facades\Schema;

class CreateArtikelsComment extends Migration
{
    /**
     * Run the migrations.
     *
     * @return void
     */
    public function up()
    {
        Schema::create('artikels_comment', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('artikel_id')->unsigned()->nullable();
            $table->foreign('artikel_id')->references('id')->on('artikel')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('comment');
            $table->integer('like')->default('0');
            $table->timestamps();
            $table->softDeletes();
        });

        Schema::create('artikels_comment_balas', function (Blueprint $table) {
            $table->id();
            $table->unsignedBigInteger('comment_id')->unsigned()->nullable();
            $table->foreign('comment_id')->references('id')->on('artikels_comment')->onDelete('cascade');
            $table->unsignedBigInteger('user_id')->unsigned()->nullable();
            $table->foreign('user_id')->references('id')->on('users')->onDelete('cascade');
            $table->text('comment');
            $table->integer('like')->default('0');
            $table->timestamps();
            $table->softDeletes();
        });
    }

    /**
     * Reverse the migrations.
     *
     * @return void
     */
    public function down()
    {
        Schema::dropIfExists('artikels_comment_balas');
        Schema::dropIfExists('artikels_comment');
    }
}
