<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\ArtikelsCommentBalas;
use Illuminate\Http\Request;

class ArtikelsCommentBalasController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function show(ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtikelsCommentBalas  $artikelsCommentBalas
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtikelsCommentBalas $artikelsCommentBalas)
    {
        //
    }
}
