<?php

namespace App\Http\Controllers\Api\Artikel;

use App\Http\Controllers\Controller;
use App\Models\ArtikelsComment;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ArtikelsCommentController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        return NULL;
        $request->validate([
            'comment' => 'required|min:10|string',
            'id' => 'required|numeric'
        ]);

        $user_d = Auth::user()->id;

        $comment = new ArtikelsComment([
            'artikel_id' => $request->id,
            'user_id' => $user_d,
            'comment' => $request->comment
        ]);
        $comment->save();
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function show(ArtikelsComment $artikelsComment)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function edit(ArtikelsComment $artikelsComment)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, ArtikelsComment $artikelsComment)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\ArtikelsComment  $artikelsComment
     * @return \Illuminate\Http\Response
     */
    public function destroy(ArtikelsComment $artikelsComment)
    {
        //
    }

    public function commentWithIdArtikel($id)
    {
       $comments = ArtikelsComment::where('artikel_id', $id)
       ->with(array('user' => function($query) {
        $query->select('id', 'username', 'photo_profile');
        }, 'balas.user' => function($query) {
        $query->select('id', 'username', 'photo_profile');
        }))
       ->get();

       return response(['comments' => $comments], 200);
    }
}
