<?php

namespace App\Http\Controllers\Api;

use App\Http\Controllers\Controller;
use App\Models\User;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Auth;

class AuthController extends Controller
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
        //
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        //
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
        //
    }

    /**
     * Display the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function show(User $user)
    {
        //
    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function edit(User $user)
    {
        //
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, User $user)
    {
        //
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  \App\Models\User  $user
     * @return \Illuminate\Http\Response
     */
    public function destroy(User $user)
    {
        //
    }

    public function register(Request $request)
    {
        $validatedData = $request->validate([
            'name' => 'required|string',
            'username' => 'required|string|unique:users',
            'phone' => 'required|string',
            'email' => 'email|required|unique:users',
            'password' => 'required|confirmed|min:8|regex:/^.*(?=.{3,})(?=.*[a-zA-Z])(?=.*[0-9])(?=.*[\d\x]).*$/',
            'password_confirmation' => 'required',
            'agreement' => 'required|boolean'
        ]);
        

        $validatedData['password'] = Hash::make($request->password);

        unset($validatedData['passwod_confirmation']);
        unset($validatedData['agreement']);
        $user = User::create($validatedData);

        $accessToken = $user->createToken('authToken')->accessToken;

        return response(['message' => 'Akunmu berhasil didaftarkan', 'user' => $user, 'access_token' => $accessToken], 200);
    }

    public function login(Request $request)
    {
        $request->validate([
            'akun' => 'required|string',
            'password' => 'required|string',
        ]);

        $akun = User::where('username', $request->akun)->first();
        if ($akun) {
            $email = $akun->email;
        } else {
            $akun = User::where('email', $request->akun)->first();
            if (!$akun) return response(['message' => 'Username atau password Anda salah'], 402);
            $email = $request->akun;
        }
        $credentials = [
            'email' => $email,
            'password' => $request->password
        ];

        if (!Auth::attempt($credentials)) {
            return response(['message' => 'Username atau password Anda salah'], 400);
        }

        $user = Auth::user();
        $accessToken = $user->createToken('authToken')->accessToken;

        return response([
            'message' => 'Kamu berhasil login',
            'user' => Auth::user(),
            'access_token' => $accessToken,
            'token_type' => 'Bearer'
        ], 200);
    }
}
