<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;
use Illuminate\Database\Eloquent\SoftDeletes;

class RegistIntHeroes extends Model
{
  use SoftDeletes;

  protected $table = 'regist_int_heroes';
  protected $softdelete;
  protected $hidden = ['updated_at', 'delete_at'];
  protected $fillable = ['full_name', 'wa_number', 'nationality', 'age', 'status', 'language_speake', 'language_teach', 'division', 'follow_ig'];
  protected $guarded = ['id'];
}
