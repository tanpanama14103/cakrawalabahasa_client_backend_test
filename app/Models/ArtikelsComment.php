<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Builder;
use Illuminate\Database\Eloquent\Model;

class ArtikelsComment extends Model
{
    protected $table = 'artikels_comment';
    protected $hidden = ['updated_at', 'deleted_at'];
    protected $fillable = ['artikel_id', 'user_id', 'comment'];

    public function artikel()
    {
        return $this->belongsTo(Artikel::class);
    }

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function balas()
    {
        return $this->hasMany(ArtikelsCommentBalas::class, 'comment_id');
    }

    protected static function booted()
    {
        // default setiap query, bila tidak ingin digunakan ->withoutGlobalScopes(['order_desc'])->get();
        static::addGlobalScope('order_desc', function (Builder $builder) {
            $builder->orderBy('id', 'desc');
        });
    }
}
