<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Model;

class ArtikelsCommentBalas extends Model
{
    protected $table = 'artikels_comment_balas';
    protected $hidden = ['updated_at', 'deleted_at'];

    public function user()
    {
        return $this->belongsTo(User::class);
    }

    public function ArtikelsComment()
    {
        return $this->belongsTo(ArtikelsComment::class, 'comment_id');
    }
}
